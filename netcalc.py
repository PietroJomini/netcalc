# CMD wrapper around network.py

from sys import argv
from ipv4 import ipv4, _ipv4_mask
from network import network
from ping import ping

USAGE = """
Choose one:
    << ipv4 Netcalc >>
    Usage: $ python3 netcalc.py ip mask smask
        - ip: Base net ip [eg: 192.168.1.1]: str as a.b.c.d | int as n
        - mask, smask: Masks of origin and destination networks [eg: \\24 \\29]: str as a.b.c.d | int as n | str as \\n
    << ipv4 ping >>
    Usage: $ python3 netcalc.py --ping ip [mask]
        - ip: Base net ip [eg: 192.168.1.1]: str as a.b.c.d | int as n
        - mask: Range mask [eg: \\24]: str as a.b.c.d | int as n | str as \\n
          if mask ping evenry net' host & net address, else only ip
    << Options >>
    usage: $ ... --option ...
        - verbose: verbose output on ping
"""


def ERROR(msg):
    print(f'\nERROR :: {msg}')


def parse_cmline(argv):

    PING = False
    if '--ping' in argv:
        PING = True
        argv.remove('--ping')

    VERBOSE = False
    if '--verbose' in argv:
        VERBOSE = True
        argv.remove('--verbose')

    if (not len(argv) == 4 and not PING) or (not len(argv) == 3 and PING and not len(argv) == 2 and PING):
        print(USAGE)
        exit(-1)

    if not PING:
        _, ip, mask, smask = argv
    elif len(argv) == 3:
        _, ip, mask = argv
    else:
        _, ip = argv
        mask = False

    try:
        ip = ipv4(ip)
    except Exception as e:
        ERROR(f'Error while parsing ip: {e}')
        print(USAGE)
        exit(-1)

    if mask:
        try:
            mask = ipv4.mask(mask)
        except Exception as e:
            ERROR(f'Error while parsing mask: {e}')
            print(USAGE)

    if not PING:
        try:
            smask = ipv4.mask(smask)
        except Exception as e:
            ERROR(f'Error while parsing smask: {e}')
            print(USAGE)
            exit(-1)
    else:
        smask = None

    return ip, mask, False, PING, VERBOSE


if __name__ == "__main__":

    ip, mask, smask, PING, VERBOSE = parse_cmline(argv)

    if not PING:
        net = network(ip, mask)
        print(f'Network:\n\tAddress: {net}\n\tBroadcast: {net.broadcast()}\n')
        subnets = list(net.mask(smask))
        print(f'\n[{len(subnets)}] Subnets:')
        for subnet in subnets:
            print(
                f'\tAddress: {subnet}\n\tBroadcast: {subnet.broadcast()}\n')
    else:
        if mask != False:
            net = network(ip, mask)
            print(
                f'Network:\n\tAddress: {net}\n\tBroadcast: {net.broadcast()}\n')
            for host in net.hosts():
                print(f'Ping {host.simple()}: ', end='')
                err, res = ping(str(host.simple()))
                if err:
                    print(f'An error occurred')
                else:
                    pack, times = res
                    if not pack or not times:
                        if not VERBOSE:
                            print(' Host not reached')
                        else:
                            print('\n\tHost not reached\n')
                    else:
                        if VERBOSE:
                            sent, received, lost, ptime = pack
                            tmin, tavg, tmax, tmdev = times
                            print(
                                f'\n\tPackage sent: {sent}\n\tPackage received: {received}\n\tPackage lost: {lost}\n\tTime: {ptime}\n')
                        else:
                            print(' Host reached')
        else:
            err, res = ping(str(ip))
            print(f'Ping {ip}: ', end='')
            if err:
                print(f'An error occurred')
            else:
                pack, times = res
                if not pack or not times:
                    if not VERBOSE:
                        print(' Host not reached')
                    else:
                        print('\n\tHost not reached\n')
                else:
                    if VERBOSE:
                        sent, received, lost, ptime = pack
                        tmin, tavg, tmax, tmdev = times
                        print(
                            f'\n\tPackage sent: {sent}\n\tPackage received: {received}\n\tPackage lost: {lost}\n\tTime: {ptime}\n')
                    else:
                        print(' Host reached')
