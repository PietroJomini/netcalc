import platform
import subprocess
import re
from sys import argv

COUNT = 1


def ping(host, count=COUNT):

    system = platform.system()
    assert system == 'Linux', f'Ping works only on Linux, but found {system}'

    out, err = subprocess.Popen(
        ['ping', f'-c  {count}', host],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    ).communicate()

    raw = out.decode()

    if raw and not err:
        general = raw.split('\n')[3:-1]
        if len(general) >= 2 and general[1] != '':
            packet = tuple((
                int(re.findall(r'[0-9]+', info)[0]) for info in general[1].split(', ')))
        else:
            packet = False
        if len(general) >= 3 and general[2] != '':
            times = tuple((float(t)
                           for t in general[2].split(' ')[3].split('/')))
        else:
            times = False

        return False, (packet, times)

    return err.decode(), None
