from network import network
from ipv4 import ipv4

net = network(ipv4('192.168.10.23'), ipv4.mask('255.255.255.0'))

assert str(net.address()) == '192.168.10.0', 'Network'
assert str(net.broadcast()) == '192.168.10.255', 'Broadcast'

subnets = list(net.mask(ipv4.mask('29')))
assert str(subnets[0].simple()) == str(net.address()), 'First subnet should be equal to net address'
assert str(subnets[-1].simple()) == '192.168.10.248', 'Last subnet address'

hosts = list(subnets[0].hosts())
assert str(hosts[0].simple()) == '192.168.10.1', 'First host of first subnet'